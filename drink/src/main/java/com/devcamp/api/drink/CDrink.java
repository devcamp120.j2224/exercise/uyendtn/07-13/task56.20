package com.devcamp.api.drink;

import java.util.Date;

public class CDrink {
    String maNuocUong;
    String tenNuocUong;
    Double donGia;
    String ghiChu;
    Date ngayTao;
    Date ngayCapNhat;

    public CDrink(String maNuocUong, String tenNuocUong, Double donGia, String ghiChu) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();
    }

    public CDrink(String maNuocUong, String tenNuocUong, Double donGia) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = "";
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();
    }

    public CDrink(String maNuocUong, String tenNuocUong) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = 15000d;
        this.ghiChu = "";
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();
    }

    public CDrink(String maNuocUong) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = "Cocacola";
        this.donGia = 15000d;
        this.ghiChu = "";
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();
    }

    public CDrink() {
        this.maNuocUong = "TRATAC";
        this.tenNuocUong = "Trà tắc";
        this.donGia = 10000d;
        this.ghiChu = "";
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();

    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public Double getDonGia() {
        return donGia;
    }

    public void setDonGia(Double donGia) {
        this.donGia = donGia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
